---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "State observer and feedback control of
electro-mechanical system with elasticities and
disturbances of payload"
summary: "Project part the course MAS508 - Control Theory,  as part of the mechatronics study program at the University of Agder."
authors: [admin, Even Falkenberg Langås, Sindre Bokneberg]
tags: [Mechatronics, UiA]
categories: []
date: 2020-12-12T15:48:03+02:00

# Optional external URL for project (replaces project detail page).
external_link: ""

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_code: ""
url_pdf: "files/MAS508___Main_Project.pdf"
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

{{< youtube 3kKyzIGQEq0 >}}
