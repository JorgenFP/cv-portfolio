+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Experience"
# subtitle = "My professional experience"

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Engineering Fellow"
  company = "CERN"
  company_url = "http://www.piedpiper.com/"
  location = "Geneva, Switzerland"
  date_start = "2021-06-01"
  date_end = ""
  description = """Engineering Fellow working in the safety, coordination and infrastructure department for the
CMS experiment. Working with experimental area management team as a project manager
and project engineer for various building and refurbishment projects on the CMS site. Main
activities are project coordination, documentation and management together with CAD design for certain projects."""

[[experience]]
  title = "Intern"
  company = "NORCE, Norwegian Research Centre"
  company_url = ""
  location = "Grimstad, Norway"
  date_start = "2021-05-01"
  date_end = "2021-09-01"
  description = """Worked as an intern for NORCE after my master’s thesis to publish an article based on
my findings in the master’s project. 

  """

[[experience]]
  title = "Mechanical Engineer"
  company = "Otechos"
  company_url = ""
  location = "Tvedestrand, Norway"
  date_start = "2017-10-01"
  date_end = "2019-08-01"
  description = """Product development and various tasks such as design, 3D CAD drawing and prototyping.
Technical project manager for some internal projects.
  """

+++
