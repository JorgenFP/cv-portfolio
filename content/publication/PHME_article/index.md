---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Forecasting Piston Rod Seal Failure Based on Acoustic Emission Features in ARIMA Model"
authors: [Jørgen Fone Pedersen, Rune Schlanbusch, Vignesh V. Shanbhag]
date: 2022-06-29T00:53:52+02:00
doi: "https://doi.org/10.36001/phme.2022.v7i1.3326"

# Schedule page publish date (NOT publication's date).
publishDate: 2022-06-29T00:53:52+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "PROGNOSTICS AND HEALTH MANAGEMENT SOCIETY"
publication_short: "PHME"

abstract: "Fluid leakage due to piston rod seal failure in hydraulic cylinders results in unscheduled maintenance, machine downtime and loss of productivity. Therefore, it is vital to understand the piston rod seal failure at initial stages. In literature, very few attempts have been made to implement forecasting techniques for piston rod seal failure in hydraulic cylinders using acoustic emission (AE) features. Therefore, in this study, we aim to forecast piston rod seal failure using AE features in the auto regressive integrated moving average (ARIMA) model. AE features like root mean square (RMS) and mean absolute percentage error (MAPE) were collected from run-to-failure (RTF) tests that were conducted on a hydraulic test rig. The hydraulic test rig replicates the piston rod movement and fluid leakage conditions similar to what is normally observed in hydraulic cylinders. To assess reliability of our study, two RTF tests were conducted at 15 mm/s and 25 mm/s rod speed each. The process of seal wear from unworn to worn state in the hydraulic test rig was accelerated by creating longitudinal scratches on the piston rod. An ARIMA model was developed based on the RMS features that were calculated from four RTF tests. The ARIMA model can forecast the RMS values ahead in time as long as the original series does not experience any large shifts in variance or deviates heavily from the normal increasing trend. The ARIMA model provided good accuracy in forecasting the seal failure in at least two of four RTF tests that were conducted. The ARIMA model that was fitted with 15 pre-samples was used to forecast 10 out of sequence samples, and it showed a maximum moving absolute percentage error (MAPE value) of 28.99 % and a minimum of 4.950 %. The forecasting technique based on ARIMA model and AE features proposed in this study lays a strong basis to be used in industries to schedule the seal change in hydraulic cylinders."

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
