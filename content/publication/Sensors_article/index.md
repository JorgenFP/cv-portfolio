---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Acoustic Emission-Based Condition Monitoring and Remaining Useful Life Prediction of Hydraulic Cylinder Rod Seals"
authors: [Jørgen F. Pedersen, Rune Schlanbusch, Thomas J. J. Meyer, Leo W. Caspers ,Vignesh V. Shanbhag]
date: 2021-09-08T01:07:33+02:00
doi: "https://doi.org/10.3390/s21186012"

# Schedule page publish date (NOT publication's date).
publishDate: 2021-09-08T01:07:33+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "Sensors"
publication_short: ""

abstract: "The foremost reason for unscheduled maintenance of hydraulic cylinders in industry is
caused by wear of the hydraulic seals. Therefore, condition monitoring and subsequent estimation of
remaining useful life (RUL) methods are highly sought after by the maintenance professionals. This
study aimed at investigating the use of acoustic emission (AE) sensors to identify the early stages
of external leakage initiation in hydraulic cylinders through run to failure studies (RTF) in a test
rig. In this study, the impact of sensor location and rod speeds on the AE signal were investigated
using both time- and frequency-based features. Furthermore, a frequency domain analysis was
conducted to investigate the power spectral density (PSD) of the AE signal. An accelerated leakage
initiation process was performed by creating longitudinal scratches on the piston rod. In addition, the
effect on the AE signal from pausing the test rig for a prolonged duration during the RTF tests was
investigated. From the extracted features of the AE signal, the root mean square (RMS) feature was
observed to be a potent condition indicator (CI) to understand the leakage initiation. In this study,
the AE signal showed a large drop in the RMS value caused by the pause in the RTF test operations.
However, the RMS value at leakage initiation is seen to be a promising CI because it appears to
be linearly scalable to operational conditions such as pressure and speed, with good accuracy, for
predicting the leakage threshold.
"

# Summary. An optional shortened abstract.
summary: ""

tags: []
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
