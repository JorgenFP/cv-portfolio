---
# Documentation: https://sourcethemes.com/academic/docs/managing-content/

title: "Acoustic Emission Based Condition Monitoring and Remaining Useful Life
Prediction of Seals in Hydraulic Cylinder"
authors: [Jørgen Fone Pedersen]
date: 2022-08-21T01:15:25+02:00
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: 2021-06-03T01:15:25+02:00

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["7"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: "This thesis has aimed to investigate the use of acoustic emission (AE) sensors to identify
the early stages of external leakage initiation in hydraulic cylinders. External fluid leakage
in hydraulic cylinders is caused by various failure modes of the hydraulic seals or the rod.
Hydraulic fluid leakage has major cost and enverimental concerns related to it. Therefore,
the condition monitoring and subsequent estimation of remaining useful life (RUL) methods
are highly sought after by maintenance professionals. The investigation has been conducted
through several studies with varying process parameters and test conditions. Varying sensor
locations, seal configurations, working pressures, rod speeds, and axial loads have all been
investigated to determine their impact on the AE signal through both time and frequency
domain analysis. The root mean square (RMS) feature shows promise in being able to
identify the rod speed and the condition of the signal transfer function between AE sensor
and cylinder, while the fluid pressure shows little impact on the RMS signal. The axial load
conditions show that they are correlated with the RMS feature for the extraction stroke, and
to some degree the retraction stroke. An accelerated leakage initiation process was created
by artificially creating longitudinal scratches on the rod. For run to failure (RTF) tests, the
RMS feature shows promise in being able to be applied directly as a condition indicator (CI)
for the leakage state. The RMS signal process has been modelled using an autoregressive
integrated moving average (ARIMA) model. To create an estimation of RUL, the forecasted
values were scaled to compensate for different test conditions. After compensating, the RMS
leakage initiation threshold was found to lie very close in value. The method shows promise
by converging towards a similar RUL estimate for most tests. However, the method is limited
by the accuracy and applicabilty of the ARIMA model for forecasting on the AE RMS signal
due to low stationarity in the signal.
"


# Summary. An optional shortened abstract.
summary: ""

tags: [Master Thesis]
categories: []
featured: false

# Custom links (optional).
#   Uncomment and edit lines below to show custom links.
# links:
# - name: Follow
#   url: https://twitter.com
#   icon_pack: fab
#   icon: twitter

url_pdf:
url_code:
url_dataset:
url_poster:
url_project:
url_slides:
url_source:
url_video:

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
# Focal points: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight.
image:
  caption: ""
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: ""
---
