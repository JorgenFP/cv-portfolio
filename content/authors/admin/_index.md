---
# Display name
name: Jørgen Fone Pedersen
avatar_image: "img/profile_picture.jpg"
# Username (this should match the folder name)
authors:
- Jørgen Fone Pedersen
# resume download button
btn:
- url : "files/cv.pdf"
  label : "Download Resume"

# Is this the primary user of the site?
superuser: true

# Role/position
role: Engineering fellow at CERN

# Organizations/Affiliations
organizations:
- name:  CERN
  url: "https://home.cern/"

# Short bio (displayed in user profile at end of posts)
bio: 

# Should the user's education and interests be displayed?
display_education: true

interests:
- Robotics
- Project management
- Arduino
- Finite Element Analysis
- CAD
- Cars and tractors


education:
  courses:
  - course: M.Sc. i Mechatronics
    institution: University of Agder
    year: 2021

  - course: B.Sc. in Mechanical design
    institution: University of South East Norway
    year: 2017

# Social/academia Networking
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
# - icon: envelope
#   icon_pack: fas
#   link: '#contact'  # For a direct email link, use "mailto:test@example.org".
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/j%C3%B8rgen-fone-pedersen-523861137/
# - icon: google-scholar
#   icon_pack: ai
#   link: https://
# - icon: github
#   icon_pack: fab
#   link: https://
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/JorgenFP
# Link to a PDF of your resume/CV from the About widget.
 #- icon: cv
  # icon_pack: ai
 #  link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""
  
# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.  
user_groups:
- Researchers
- Visitors
---

About me: 30 year old mechatronics engineer from Norway. Currently working at CERN in Switzerland and France working with the CMS collaboration. On the lookout for a job in southern Norway after my contract finishes with CERN summer 2023. Feel free to reach out if you are interested to hear more on what I can offer to your company. 


<!-- ![reviews](img/certifacates.jpg) -->

